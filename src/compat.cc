#include <ocaml-boot-riscv/print.h>
#include <ocaml-boot-riscv/htif.h>
#include <ocaml-boot-riscv/config.h>
#include <ocaml-boot-riscv/compat.h>
#include <ocaml-boot-riscv/timer.h>

extern "C" {
    typedef unsigned long long time_t;

    void riscv_poweroff(int status){
        pk::htif_poweroff();
    }
    void riscv_write(const char* s, unsigned int length) {
        util::putstring(s, length);
    }
    time_t riscv_clock_monotonic() {
        volatile unsigned long* tmp = (volatile unsigned long*)config::mtime;
        return (time_t) *tmp;
    }

    void riscv_wait(time_t delay) {
        time_t curr = riscv_clock_monotonic();
        time_t next = curr+delay;
        pk::set_timer(next);
        while(!pk::timer_pending()){
            asm volatile("wfi");
        }
        pk::clear_timer();
    }
}
